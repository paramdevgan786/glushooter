﻿using UnityEngine;
/// <summary>
/// this scrip can be used to draw some specific shapes, it works with line render to draw the path and from there we can save the path coordinates
/// </summary>
public class ShapeCreator : MonoBehaviour
{
    // Start is called before the first frame update
    #region public vars
    public FormationCoordinates formationCoordinatesInstance;
    [Header("Select the shape type before creating any new shape")]
    public ShapeType shapeType;
    #endregion

    #region private vars
    private LineRenderer lineRenderer;
    #endregion
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();

    }


    public void SaveCoordinates()
    {
        lineRenderer = GetComponent<LineRenderer>();
        Vector3[] newPos = new Vector3[lineRenderer.positionCount];
        lineRenderer.GetPositions(newPos);
        ShapeData shape = new ShapeData("First", shapeType, newPos);

        switch (shapeType)
        {

            case ShapeType.Simple:
                formationCoordinatesInstance.simpleShapes.Add(shape);
                break;

            case ShapeType.Medium:
                formationCoordinatesInstance.mediumShapes.Add(shape);
                break;

            case ShapeType.Complex:
                formationCoordinatesInstance.complexShapes.Add(shape);
                break;


        }



    }
}
