﻿using TMPro;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;
/// <summary>
/// GUI Manager handles gameplay screen UI stuff, various UI panels etc
/// </summary>
public class GUIManager : MySingleton<GUIManager>
{
    #region public fields
    public Image HealthBar;
    public TextMeshProUGUI ScoreText;
    public Transform LevelClearedPanel;
    public Transform LevelFailedPanel;

    #endregion


    #region private FIELDS
    private int score;
    private bool isGameOver;
    #endregion

    public void LevelCompleted()
    {
        if (HealthBar.fillAmount > 0)
        {
            StartCoroutine(checkForEnemy());//we are checking if still any enemy is alive on screen

        }
    }

    public void LevelFailed()
    {
        if (!isGameOver)
        {
            isGameOver = true;
            LevelFailedPanel.gameObject.SetActive(true);
            UpdateHighScore();
            Debug.Log("Level Failed");
        }
    }


    public void UpdateHealth(float damage)
    {
        if (HealthBar.fillAmount > 0)
        {
            HealthBar.fillAmount = damage;
        }
    }


    public void UpdateScore(int scoreVal)
    {
        score += scoreVal;
        ScoreText.text = score + "";
    }


    public void UpdateHighScore()
    {
        int highscore = PlayerPrefs.GetInt("HighScore");
        if (score > highscore)
        {
            highscore = score;
            PlayerPrefs.SetInt("HighScore", highscore);
        }
    }

    public void MainMenuBtn()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }

    #region private methods


    private IEnumerator checkForEnemy()
    {
        EnemyBehaviour[] enemies = FindObjectsOfType<EnemyBehaviour>();
        yield return new WaitForSeconds(1f);
        if (enemies.Length > 0)
        {
            StartCoroutine(checkForEnemy());
        }
        else
        {
            updateLevelCleared();
            StopCoroutine(checkForEnemy());
        }

    }

    private void updateLevelCleared()
    {
        UpdateHighScore();
        LevelClearedPanel.gameObject.SetActive(true);

    }
    #endregion
}
