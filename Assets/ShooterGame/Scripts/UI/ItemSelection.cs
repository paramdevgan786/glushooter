﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ItemSelection : MonoBehaviour
{
    public TextMeshProUGUI ItemIndex;
    private int itemIndex;

    public void SetIndexVale(int index)
    {
        itemIndex = index;
        ItemIndex.text = itemIndex + "";
    }

    public void SelectCurrentItem()
    {
        PlayerPrefs.SetInt("SelectedLevel", itemIndex);
        Debug.Log("The Selected Level Is: " + itemIndex);
        SceneManager.LoadScene("GamePlay");

    }
}
