﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// menu manager handles game menu UI canvas elements
/// </summary>
public class MenuManager : MonoBehaviour
{
    #region public fields
    public Transform MenuPanel;
    public Transform LevelSelectionPanel;
    public Transform LevelItemPrefab;
    public Transform ScrollContent;
    public TextMeshProUGUI HighScoreText;
    #endregion


    #region private Fields
    private int levelsInList = 5;
    #endregion


    #region private methods
    private void Start()
    {
        setPlayerPref();
        updateMenuTexts();
        createLevelScrollList();
    }
    /// <summary>
    ///
    /// set player prefs
    /// </summary>
    private void setPlayerPref()
    {
        if (!PlayerPrefs.HasKey("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", 0);
        }
        if (!PlayerPrefs.HasKey("SelectedLevel"))
        {
            PlayerPrefs.SetInt("SelectedLevel", 1);// by default starting from level1
        }
    }
    /// <summary>
    /// In this method we can update all display text values
    /// </summary>
    private void updateMenuTexts()
    {
        HighScoreText.text = "High Score: " + PlayerPrefs.GetInt("HighScore");
    }

    /// <summary>
    /// create dynamic list of level items we can increase or decrease the size of  list based off available levels in game
    /// </summary>
    private void createLevelScrollList()
    {
        int totalItems = levelsInList;

        for (int index = 0; index < totalItems; index++)
        {
            GameObject gobj = Instantiate(LevelItemPrefab.gameObject);
            gobj.GetComponent<ItemSelection>().SetIndexVale((index + 1));
            gobj.transform.SetParent(ScrollContent, false);

        }

    }

    #endregion

    #region public Methods
    public void PlayBtnClick()
    {
        LevelSelectionPanel.gameObject.SetActive(true);
        MenuPanel.gameObject.SetActive(false);
    }

    public void BackBtnClick()
    {
        LevelSelectionPanel.gameObject.SetActive(false);
        MenuPanel.gameObject.SetActive(true);
    }
    #endregion
}
