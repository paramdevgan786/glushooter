﻿using UnityEngine;

/// <summary>
/// more oriedented towards recieving damage from enemies
/// </summary>
public class PlayerHealthController : MonoBehaviour, IPlayerDamagable
{
    #region public fields
    [Header("Player Health")]
    public int Health;
    [Header("Player Explosion Effect")]
    public ParticleSystem PlayerExplosion;
    #endregion

    #region private fields
    private int totalHealth;//used at the start to cache the total health
    #endregion

    //================interface implementation=======//
    public bool canTakeDamage { get; set; }
    public void GetDamage(int damage)
    {
        if (Health > 0)
        {
            if (canTakeDamage)//no shield is activated yet
            {
                Health -= damage;
                updateHealthBar();
            }
        }
        else//no health
        {
            destruction();
        }
    }
    //=============End of Interface implementation=======//


    //=================Script METHODS================//



    private void Start()
    {
        canTakeDamage = true;//there is no shield actiavted yet so by default can take damage
        totalHealth = Health;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PowerUp>() != null)
        {
            PowerUpType pType = other.GetComponent<PowerUp>().powerUpType;
            PowerUpController.Instance.UpdatePowerUp(pType);
            Destroy(other.gameObject);
        }
    }

    private void destruction()
    {
        //Show particle effect here
        Instantiate(PlayerExplosion.gameObject, transform.position, Quaternion.identity);
        GUIManager.Instance.LevelFailed();
        Destroy(gameObject);

    }

    private void updateHealthBar()
    {
        float healthFloatVal = (float)Health / totalHealth;
        GUIManager.Instance.UpdateHealth(healthFloatVal);
    }


    //=============================================//
}
