﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script only handles the Input gestures and based of that handled player movement
/// </summary>
public class PlayerMovementController : MonoBehaviour
{
    #region private sector
    private Camera mainCamera;
    private bool isGameOver = false;
    private Vector3 dragOffset;
    #endregion

    private void Start()
    {
        mainCamera = Camera.main;
    }

    private void Update()
    {
        if (!isGameOver)
        {
#if UNITY_STANDALONE || UNITY_EDITOR    //ONPC

            if (Input.GetMouseButtonDown(0))
            {
                Vector3 intialPosition = Input.mousePosition;
                Vector3 initialWordlPos = Camera.main.ScreenToWorldPoint(intialPosition);
                dragOffset = transform.position - initialWordlPos;
                dragOffset.y = 0;
            }

            if (Input.GetMouseButton(0)) //if mouse button was pressed       
            {

                Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition) + dragOffset; //calculating mouse position in the worldspace
                mousePosition.y = 0;//transform.position.y;
                transform.position = Vector3.MoveTowards(transform.position, mousePosition, 50 * Time.deltaTime);
            }
#endif

#if UNITY_IOS || UNITY_ANDROID //ON Mobile

            if (Input.touchCount > 0) // if there is a touch
            {
                Touch touch = Input.touches[0];
                Vector3 touchPosition = mainCamera.ScreenToWorldPoint(touch.position);  //calculating touch position in the world space
                                                                                        //  touchPosition.y = 0;

                switch (touch.phase)
                {

                    case TouchPhase.Began:
                        Vector3 intialPosition = touch.position;
                        Vector3 initialWordlPos = Camera.main.ScreenToWorldPoint(intialPosition);
                        dragOffset = transform.position - initialWordlPos; dragOffset.y = 0;

                        break;

                    case TouchPhase.Moved:
                        touchPosition = mainCamera.ScreenToWorldPoint(touch.position) + dragOffset;  //calculating touch position in the world space
                        touchPosition.y = 0;
                        transform.position = Vector3.MoveTowards(transform.position, touchPosition, 50 * Time.deltaTime);
                        break;


                }

            }
#endif

            transform.position = new Vector3    //making the player to remain with in boundary bounds
                (
                Mathf.Clamp(transform.position.x, ScreenBoundary.Instance.MinX, ScreenBoundary.Instance.MaxX),
                transform.position.y,
                Mathf.Clamp(transform.position.z, ScreenBoundary.Instance.MinY, ScreenBoundary.Instance.MaxY)

                );
        }
    }

}
