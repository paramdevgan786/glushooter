﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// giving damage to enemies
/// </summary>
public class PlayerMissileBehaviour : MonoBehaviour
{
    [Header("Damage To Enemy")]
    public int DamageValue = 20;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<IEnemyDamagable>() != null)
        {
            IEnemyDamagable enemyDamagable = other.gameObject.GetComponent<IEnemyDamagable>();
            enemyDamagable.GetDamage(DamageValue);
            PoolManager.Instance.addInactiveMissile(gameObject);
        }
    }
}
