﻿using UnityEngine;
/// <summary>
/// This script handles player shooting (shooting from various turret, the speed, the power shoot boost etc)
/// </summary>
public class PlayerShootingController : MonoBehaviour
{

    [System.Serializable]
    public class MissileTurret
    {
        public GameObject rightTurret, leftTurret, centralTurret;
    }


    [Header("shooting frequency. the higher the more frequent")]
    public float fireRate;

    [Header("missile prefab")]
    public GameObject projectileObject;
    public GameObject playerShield;

    [Header("duration between shots")]
    public float nextFire;

    [Header("Assign Plane Turrets here")]
    public MissileTurret missileTurret;

    #region PRIVATE fields
    private IPlayerDamagable playerDamagable;
    private bool isShootingBoostActive;
    private float increaseInFireRate = 5.0f;//we will add this value to the exiting fire rate only for the time when shoot boost is active
    #endregion

    /// <summary>
    /// subscribing events 
    /// </summary>
    private void OnEnable()
    {
        PowerUpController.Instance.EnablePowerUp += activatePowerBoost;
        PowerUpController.Instance.DisablePowerUp += deactivatePowerBoost;
    }

    private void OnDisable()
    {
        if (PowerUpController.Instance != null)
        {
            PowerUpController.Instance.EnablePowerUp -= activatePowerBoost;
            PowerUpController.Instance.DisablePowerUp -= deactivatePowerBoost;
        }
    }

    private void activatePowerBoost(PowerUpType powerUpType)
    {
        switch (powerUpType)
        {
            case PowerUpType.Shield:

                playerShield.SetActive(true);
                playerDamagable.canTakeDamage = false;

                break;

            case PowerUpType.ShootBoost:

                fireRate += increaseInFireRate;
                isShootingBoostActive = true;

                break;
        }


    }

    private void deactivatePowerBoost(PowerUpType powerUpType)
    {
        switch (powerUpType)
        {
            case PowerUpType.Shield:
                playerShield.SetActive(false);
                playerDamagable.canTakeDamage = true;
                break;

            case PowerUpType.ShootBoost:
                fireRate -= increaseInFireRate;
                isShootingBoostActive = false;
                break;
        }
    }

    private void Start()
    {
        playerDamagable = GetComponent<IPlayerDamagable>();
    }

    private void Update()
    {
        if (Time.time > nextFire)
        {
            Shooting();
            nextFire = Time.time + 1 / fireRate;
        }
    }

    //plane shooting
    private void Shooting()
    {
        if (isShootingBoostActive)
        {
            CreateMissileShot(PoolManager.Instance.getMissiles(), missileTurret.centralTurret.transform.position, new Vector3(0, 0, 0));
        }
        CreateMissileShot(PoolManager.Instance.getMissiles(), missileTurret.rightTurret.transform.position, new Vector3(0, 0, 0));
        CreateMissileShot(PoolManager.Instance.getMissiles(), missileTurret.leftTurret.transform.position, new Vector3(0, -0, 0));
        CreateMissileShot(PoolManager.Instance.getMissiles(), missileTurret.leftTurret.transform.position, new Vector3(0, -4, 0));
        CreateMissileShot(PoolManager.Instance.getMissiles(), missileTurret.rightTurret.transform.position, new Vector3(0, 4, 0));

    }
    //missile generation 
    private void CreateMissileShot(GameObject lazerM, Vector3 pos, Vector3 rot)
    {
        lazerM.transform.position = pos;
        lazerM.transform.rotation = Quaternion.Euler(rot);
        lazerM.SetActive(true);
    }

}

