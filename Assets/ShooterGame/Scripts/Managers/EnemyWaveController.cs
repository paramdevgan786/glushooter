﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
/// <summary>
/// this script controls the waves of enemy ships, like formation and timing etc
/// </summary>
public class EnemyWaveController : MonoBehaviour
{
    #region public vars
    public float StartWait;
    [Tooltip("This is the wait between the creation of enemies in each random wave")]
    public float SpawnWait;

    public FormationCoordinates formationCoordinates;
    public CurvedCoordinates curvedCoordinates;
    public DifficultyManager difficultyManager;

    #endregion

    #region private vars
    private int waveCount = 0;//used to increment the count of waves
    private int totalWaves;//stores total number of waves
    private List<Action> levelWiseActions = new List<Action>();
    private List<float> dividedScreenBounds = new List<float>();
    private Action waveTypeAction;
    private int levelIndex = 1;//used to cache the level index selected from playerpref value
    private int enemyCreationYPos = 11;//the top position outside the view port --where we create the enemies
    private float WaveWait;//wait time between waves
    #endregion

    private void Start()
    {
        levelIndex = (PlayerPrefs.GetInt("SelectedLevel") - 1);//since we are getting the level diifficulty from an array which starts from 0, so just seubtracting 1 from any level selected value
        CacheLevelData();
        StartCoroutine(SpawnWaves());
    }

    /// <summary>
    /// In the code we have divided the screen equally into four segments, the x cordinates can be used to random allocate ships on x axis
    /// </summary>
    private void saveScreenDivisionBounds()
    {
        float width = ScreenBoundary.Instance.GetBounds().x;
        float center = 0.0f;
        float extremeLeft = (width * 2) / 3;
        float midLeft = (width * 1) / 3;
        float extremeRight = extremeLeft * (-1);
        float midRight = midLeft * (-1);

        dividedScreenBounds.Add(center);
        dividedScreenBounds.Add(midLeft);
        dividedScreenBounds.Add(midRight);
        dividedScreenBounds.Add(extremeLeft);
        dividedScreenBounds.Add(extremeRight);


    }
    /// <summary>
    /// In this method we are storing the action respective to generate individual wave in the current level
    /// </summary>

    private void CacheLevelData()
    {
        WaveWait = difficultyManager.GetWaveWaitTime();
        //  Debug.Log(WaveWait);

        int actionCount = difficultyManager.levelDifficulty[levelIndex].difficultyData.Length;

        for (int index = 0; index < actionCount; index++)
        {
            DifficultyActionType type = difficultyManager.levelDifficulty[levelIndex].difficultyData[index].waveActionType;
            int occurenceOfAction = difficultyManager.levelDifficulty[levelIndex].difficultyData[index].occurenceOfAction;

            switch (type)
            {
                case DifficultyActionType.RandomWave:
                    waveTypeAction = RandomFormation;
                    break;

                case DifficultyActionType.SimpleWave:
                    waveTypeAction = SimpleShapeFormation;
                    break;

                case DifficultyActionType.CurvedWave:
                    waveTypeAction = CurveBasedFormation;
                    break;
                case DifficultyActionType.ComplexCurvedWave:
                    waveTypeAction = ComplexCurveWaves;
                    break;
                case DifficultyActionType.HeavyShipWave:
                    waveTypeAction = CreateHeavyShip;
                    break;

                case DifficultyActionType.BossWave:
                    waveTypeAction = CreateBossShip;
                    break;

            }
            for (int occurenceIndex = 0; occurenceIndex < occurenceOfAction; occurenceIndex++)
            {
                levelWiseActions.Add(waveTypeAction);
            }

        }
        totalWaves = levelWiseActions.Count;
        // Shuffle(levelWiseActions);

    }
    /// <summary>
    /// This routine is responsible for calling action to create different types of waves
    /// </summary>
    /// <returns></returns>
    private IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(StartWait);

        while (waveCount < totalWaves)
        {
            saveScreenDivisionBounds();
            levelWiseActions[waveCount]();
            yield return new WaitForSeconds(WaveWait);
            waveCount += 1;
            if (waveCount == totalWaves)
            {
                GUIManager.Instance.LevelCompleted();
                break;
            }
        }
    }

    #region creation of object based off shape and randomness
    /// <summary>
    /// simple shapes to draw here
    /// </summary>
    private void SimpleShapeFormation()
    {
        int randomShapeIndex = GetRandomNumber(0, formationCoordinates.simpleShapes.Count);
        for (int index = 0; index < formationCoordinates.simpleShapes[randomShapeIndex].pathPositions.Length; index++)
        {
            GameObject enemyShip = PoolManager.Instance.getShipTypeA();
            Vector3 spawnPosition = formationCoordinates.simpleShapes[randomShapeIndex].pathPositions[index];
            createObject(enemyShip, spawnPosition);
        }
    }
    /// <summary>
    /// complex curved shaped can be drawn from here
    /// </summary>
    private void ComplexCurveWaves()
    {
        int randomShapeIndex = GetRandomNumber(0, curvedCoordinates.complexCurvedShapes.Length);
        Instantiate(curvedCoordinates.complexCurvedShapes[randomShapeIndex]);

    }

    /// <summary>
    /// curved shaped can be drawn from here
    /// </summary>
    private void CurveBasedFormation()
    {
        int randomShapeIndex = GetRandomNumber(0, curvedCoordinates.easyCurvedShapes.Length);
        Instantiate(curvedCoordinates.easyCurvedShapes[randomShapeIndex]);

    }

    /// <summary>
    /// random creation of enemies
    /// </summary>
    private void RandomFormation()
    {
        CreateFastShip();//at the extreme left and right side ports
        StartCoroutine(randomFormationRoutine());
    }
    private IEnumerator randomFormationRoutine()
    {
        int randomShapeIndex = GetRandomNumber(2, 6);
        for (int index = 0; index < randomShapeIndex; index++)
        {
            GameObject enemyShip = PoolManager.Instance.getShipTypeB();
            Vector3 spawnPosition = new Vector3(dividedScreenBounds[GetRandomNumber(0, 3)], 0, enemyCreationYPos);
            createObject(enemyShip, spawnPosition);
            yield return new WaitForSeconds(SpawnWait);
        }

    }
    private void CreateFastShip()
    {
        int end = GetRandomNumber(1, 5);
        for (int index = 0; index < end; index++)
        {
            GameObject enemyShip = PoolManager.Instance.getShipTypeC();
            Vector3 spawnPosition = new Vector3(dividedScreenBounds[GetRandomNumber(3, 5)], 0, enemyCreationYPos + index);//creating something on extreme left sides
            createObject(enemyShip, spawnPosition);
        }
    }


    private void CreateHeavyShip()
    {
        int randomShapeIndex = GetRandomNumber(1, 3);
        Vector3 spawnPosition = Vector3.zero;
        for (int index = 0; index < randomShapeIndex; index++)
        {
            GameObject enemyShip = PoolManager.Instance.getHeavyShip();
            if (index > 0)
                spawnPosition = new Vector3(dividedScreenBounds[3], 0, enemyCreationYPos);
            else
                spawnPosition = new Vector3(dividedScreenBounds[4], 0, enemyCreationYPos);

            createObject(enemyShip, spawnPosition);

        }
    }

    private void CreateBossShip()
    {
        GameObject enemyShip = PoolManager.Instance.getBossShip();
        Vector3 spawnPosition = new Vector3(dividedScreenBounds[0], 0, enemyCreationYPos);
        createObject(enemyShip, spawnPosition);
    }



    #endregion



    #region ====INTERNAL METHODS======just supporting methods for code

    private void createObject(GameObject gobj, Vector3 pos)
    {
        gobj.transform.position = pos;
        gobj.transform.rotation = Quaternion.AngleAxis(180, Vector3.up);
        gobj.SetActive(true);
    }


    private void Shuffle(List<Action> list)
    {
        int count = list.Count;
        int lastIndex = count - 1;
        for (int index = 0; index < lastIndex; ++index)
        {
            int random = UnityEngine.Random.Range(index, count);
            var temp = list[index];
            list[index] = list[random];
            list[random] = temp;
        }
    }

    private int GetRandomNumber(int minVal, int maxVal)
    {
        return UnityEngine.Random.Range(minVal, maxVal);
    }
    #endregion====End INTERNAL======

}
