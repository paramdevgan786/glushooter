﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This script handled the pooled queue for all used elements(The Enemy Planes and Bullets etc)
/// </summary>
public class PoolManager : MySingleton<PoolManager>
{
    #region public fields
    public Ships EnemyShips;
    public GameObject PlayerMissilePrefab;
    #endregion

    #region private fields
    private Queue<GameObject> ShipTypeA = new Queue<GameObject>();
    private Queue<GameObject> ShipTypeB = new Queue<GameObject>();
    private Queue<GameObject> ShipTypeC = new Queue<GameObject>();
    private Queue<GameObject> HeavyShips = new Queue<GameObject>();
    private Queue<GameObject> BossShips = new Queue<GameObject>();
    private Queue<GameObject> PlayerMissiles = new Queue<GameObject>();
    #endregion


    // Start is called before the first frame update
    void Start()
    {
        createPool();
    }
    /// <summary>
    /// create pool==we are already instanting multiple values of elements, will just enable and disble them in game play
    /// </summary>
    private void createPool()
    {
        generateShipTypeA();
        generateShipTypeB();
        generateShipTypeC();
        generateBossShipType();
        generateHeavyShipType();
        generatePlayerMissiles();
    }

    void generateShipTypeA()
    {
        for (int i = 0; i < 30; i++)
        {
            GameObject gObj = Instantiate(EnemyShips.EnemyShipA);
            gObj.SetActive(false);
            ShipTypeA.Enqueue(gObj);
        }
    }

    void generateShipTypeB()
    {
        for (int i = 0; i < 30; i++)
        {
            GameObject gObj = Instantiate(EnemyShips.EnemyShipB);
            gObj.SetActive(false);
            ShipTypeB.Enqueue(gObj);
        }
    }

    void generateShipTypeC()
    {
        for (int i = 0; i < 30; i++)
        {
            GameObject gObj = Instantiate(EnemyShips.EnemyShipC);
            gObj.SetActive(false);
            ShipTypeC.Enqueue(gObj);
        }
    }

    void generateHeavyShipType()
    {
        for (int i = 0; i < 5; i++)
        {
            GameObject gObj = Instantiate(EnemyShips.EnemyHeavyShip[UnityEngine.Random.Range(0, EnemyShips.EnemyHeavyShip.Length)]);
            gObj.SetActive(false);
            HeavyShips.Enqueue(gObj);
        }
    }

    void generateBossShipType()
    {
        for (int i = 0; i < 5; i++)
        {
            GameObject gObj = Instantiate(EnemyShips.EnemyBossShip[UnityEngine.Random.Range(0, EnemyShips.EnemyBossShip.Length)]);
            gObj.SetActive(false);
            BossShips.Enqueue(gObj);
        }
    }

    void generatePlayerMissiles()
    {
        for (int i = 0; i < 200; i++)
        {
            GameObject gObj = Instantiate(PlayerMissilePrefab);
            gObj.SetActive(false);
            PlayerMissiles.Enqueue(gObj);
        }
    }

    #region public Methods

    //========SHIP Type A GET AND SET=======//
    public void addInactiveShipTypeA(GameObject ship)
    {

        ship.SetActive(false);
        ShipTypeA.Enqueue(ship);
    }

    public GameObject getShipTypeA()
    {
        return ShipTypeA.Dequeue();
    }
    //======================================//



    //========SHIP Type B GET AND SET=======//
    public void addInactiveShipTypeB(GameObject ship)
    {

        ship.SetActive(false);
        ShipTypeB.Enqueue(ship);
    }

    public GameObject getShipTypeB()
    {
        return ShipTypeB.Dequeue();
    }
    //======================================//



    //========SHIP Type C GET AND SET=======//
    public void addInactiveShipTypeC(GameObject ship)
    {

        ship.SetActive(false);
        ShipTypeC.Enqueue(ship);
    }

    public GameObject getShipTypeC()
    {
        return ShipTypeC.Dequeue();
    }
    //======================================//




    //========SHIP Type HEAVY GET AND SET=======//
    public void addInactiveHeavyShip(GameObject ship)
    {

        ship.SetActive(false);
        HeavyShips.Enqueue(ship);
    }

    public GameObject getHeavyShip()
    {
        return HeavyShips.Dequeue();
    }
    //======================================//





    //========SHIP Type BOSS GET AND SET=======//
    public void addInactiveBossShip(GameObject ship)
    {

        ship.SetActive(false);
        BossShips.Enqueue(ship);
    }

    public GameObject getBossShip()
    {
        return BossShips.Dequeue();
    }
    //======================================//


    //========SHIP Type BOSS GET AND SET=======//
    public void addInactiveMissile(GameObject missile)
    {

        missile.SetActive(false);
        PlayerMissiles.Enqueue(missile);
    }

    public GameObject getMissiles()
    {
        return PlayerMissiles.Dequeue();
    }
    //======================================//



    #endregion
}
