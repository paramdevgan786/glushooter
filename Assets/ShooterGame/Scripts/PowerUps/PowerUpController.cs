﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
/// <summary>
/// controls the power ups activation, their active time and their trigger events
/// </summary>
public class PowerUpController : MySingleton<PowerUpController>
{
    #region FIELDS
    [Header("Timer to run power up--for how long it will remain")]
    public float powerUpRunTime;
    [Header("Tag power prefabs")]
    public PowerPrefabs powerPrefabs;
    [Header("Duration after that we want to spawn one of the powers")]
    public float powerUpSpawnDuration;
    [Header("Here we can tag evrything related to power UI")]
    [SerializeField]
    private PowerupUI[] powerUpUI;
    #endregion

    [System.Serializable]
    public class PowerupUI
    {
        public Button button;//the button to activate the power
        public Image powerFillTimerImage;//the image fill around button to tell the time
        public PowerUpType powerUpType;//the power up type
        [HideInInspector]
        public bool isActive;//it is used to detect interally if the power is already active or not
    }

    [System.Serializable]
    public struct PowerPrefabs
    {
        public Transform[] simplePowerUps;
        //In the struct you can have more power prefabs array and spawn them randomly from each array based of level complexity
    }

    public delegate void OnEnablePowerUp(PowerUpType type);
    public event OnEnablePowerUp EnablePowerUp = delegate { };

    public delegate void OnDisablePowerUp(PowerUpType type);
    public event OnDisablePowerUp DisablePowerUp = delegate { };



    //start here
    private void Start()
    {
        assignPowerUpActions();
        StartCoroutine(spawnPowerPrefabs());
    }
    //end less routine
    private IEnumerator spawnPowerPrefabs()
    {
        while (true)
        {
            yield return new WaitForSeconds(powerUpSpawnDuration);
            if (Random.Range(0, 2) > 0)
            {
                GameObject power = powerPrefabs.simplePowerUps[Random.Range(0, powerPrefabs.simplePowerUps.Length)].gameObject;
                Vector3 spawnPosition = new Vector3(Random.Range(ScreenBoundary.Instance.MinX, ScreenBoundary.Instance.MaxX), 0, 11);//creating something between extreme left sides
                Instantiate(power, spawnPosition, Quaternion.identity);
            }

        }
    }

    /// <summary>
    /// assigning listners to the power activate buttons
    /// </summary>
    private void assignPowerUpActions()
    {
        for (int index = 0; index < powerUpUI.Length; index++)
        {
            int powerPrefabIndex = index;
            powerUpUI[powerPrefabIndex].button.onClick.AddListener(() => activatePowerBoost(powerPrefabIndex));

        }
    }
    /// <summary>
    /// added this listener to all the power buttons taged
    /// </summary>
    /// <param name="index"></param>

    private void activatePowerBoost(int index)
    {
        powerUpUI[index].isActive = true;
        EnablePowerUp(powerUpUI[index].powerUpType);//sending events from here
        powerUpUI[index].button.interactable = false;
        StartCoroutine(reduceFillTimer(index));
    }


    private IEnumerator reduceFillTimer(int powerIndex)
    {

        Image timeFillImage = powerUpUI[powerIndex].powerFillTimerImage;
        float waitInEachIteration = .1f;//we can reduce this parameter to increase smothness of fill time bar
        float timer = powerUpRunTime;
        while (timer > 0)
        {
            timeFillImage.fillAmount -= (1 / timer) * waitInEachIteration;
            yield return new WaitForSeconds(waitInEachIteration);

            if (timeFillImage.fillAmount <= 0)
            {
                resetPowerBtn(powerIndex);
                break;
            }
        }
    }

    private void resetPowerBtn(int index)
    {
        powerUpUI[index].button.interactable = false;
        powerUpUI[index].isActive = false;
        DisablePowerUp(powerUpUI[index].powerUpType);

    }


    #region public Methods
    /// <summary>
    /// This method is getting called by the player script on achieving any power up
    /// </summary>
    /// <param name="type"></param>
    public void UpdatePowerUp(PowerUpType type)
    {
        for (int index = 0; index < powerUpUI.Length; index++)
        {
            if (powerUpUI[index].powerUpType == type && !powerUpUI[index].isActive)
            {
                powerUpUI[index].button.interactable = true;
                powerUpUI[index].powerFillTimerImage.fillAmount = 1;
            }
        }
    }

    #endregion


}
