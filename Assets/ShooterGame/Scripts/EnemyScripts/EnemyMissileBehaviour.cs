﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMissileBehaviour : MonoBehaviour
{

    [Header("Damage which this Missile deals to another object")]
    public int damage;


    private void OnTriggerEnter(Collider other) //
    {
        if (other.tag.Contains("Player"))
        {
            if (other.gameObject.GetComponent<IPlayerDamagable>() != null)
            {
                IPlayerDamagable playerDamagable = other.gameObject.GetComponent<IPlayerDamagable>();
                playerDamagable.GetDamage(damage);
                Destruction();
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {

        if (other.tag.Contains("EnemyDestroyer"))
        {
            Destruction();
        }

    }

    void Destruction()
    {
        Destroy(gameObject);
    }
}
