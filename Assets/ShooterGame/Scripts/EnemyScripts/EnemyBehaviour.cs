﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyBehaviour : MonoBehaviour, IEnemyDamagable
{

    #region public FIELDS

    [Header("Level Diffiulty Instance")]
    public DifficultyManager difficultyManager;

    [Header("Health Value")]
    public EnemyAbility enemyAbility;

    [Header("Enemy's Missile prefab")]
    public GameObject Projectile;

    [Header("VFX prefab generating after destruction")]
    public GameObject DestructionVFX;
    public ParticleSystem HitEffect;

    [HideInInspector] public int shotChance; //probability of 'Enemy's' shooting during tha path
    [HideInInspector] public float shotTimeMin, shotTimeMax; //max and min time for shooting from the beginning of the path
    #endregion

    #region private vars
    private int health;
    private int damageToPlayer;
    private GenericMovement movement;

    #endregion

    private void Start()
    {
        shotChance = enemyAbility.shipSettings.ShotProbability;
        health = enemyAbility.shipSettings.Health;
        damageToPlayer = enemyAbility.shipSettings.DamageToPlayer;
        Invoke("ActivateShooting", Random.Range(shotTimeMin, shotTimeMax));
        updateLevelWiseEnemyParameters();
    }
    /// <summary>
    /// let suppose selected level is 4 and we multiple this with the increment value(lets say) 100
    /// new rise in health level wise =4*100
    /// now updated health of Enemy= Default Health(EnemyWise)+400 
    /// </summary>
    private void updateLevelWiseEnemyParameters()
    {
        int levelIndex = PlayerPrefs.GetInt("SelectedLevel");
        health += (difficultyManager.IncreaseInEnemyHealth * levelIndex);// adding rise in health to original health
        shotChance += (difficultyManager.IncreaseInShootingProbability * levelIndex);//adding shot probablity to deafult one

        if (GetComponent<GenericMovement>() != null)
        {
            movement = GetComponent<GenericMovement>();
            float speed = movement.speed + (difficultyManager.IncreaseInShipSpeed * levelIndex);
            movement.UpdateSpeed(speed);
        }
    }


    //routine making a shot
    private void ActivateShooting()
    {
        if (Random.value < (float)shotChance / 100)                             //if random value less than shot probability, making a shot
        {
            Instantiate(Projectile, gameObject.transform.position, Quaternion.identity);
        }
    }

    //method of getting damage for the 'Enemy'
    public void GetDamage(int damage)
    {
        health -= damage;           //reducing health for damage value, 
        if (health <= 0)
        {
            GUIManager.Instance.UpdateScore(enemyAbility.shipSettings.scoreForPlayer);//enemy got killed by player so we are adding score to player's account
            Destruction();
        }
        else
        {
            if (!HitEffect.isPlaying)
            {
                HitEffect.Play();
            }
        }
    }

    //method of destroying the 'Enemy'
    private void Destruction()
    {
        Instantiate(DestructionVFX, transform.position, Quaternion.identity);
        disableEnemy();

    }
    /// <summary>
    /// giving damage to player plane on collision
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other) //
    {
        if (other.tag.Contains("Player"))
        {
            if (other.gameObject.GetComponent<IPlayerDamagable>() != null)
            {
                IPlayerDamagable playerDamagable = other.gameObject.GetComponent<IPlayerDamagable>();
                playerDamagable.GetDamage(damageToPlayer);
            }
        }
    }
    /// <summary>
    /// here we are just checking if plane reaches out of view port then we will deque it
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Contains("EnemyDestroyer"))
        {
            disableEnemy();
        }

    }
    /// <summary>
    /// disabling the enemies into their respective queues
    /// </summary>
    private void disableEnemy()
    {
        switch (enemyAbility.shipSettings.enemyType)
        {
            case EnemyType.ShipA:
                PoolManager.Instance.addInactiveShipTypeA(gameObject);
                break;
            case EnemyType.ShipB:
                PoolManager.Instance.addInactiveShipTypeB(gameObject);
                break;
            case EnemyType.ShipC:
                PoolManager.Instance.addInactiveShipTypeC(gameObject);
                break;
            case EnemyType.BossShip:
                PoolManager.Instance.addInactiveBossShip(gameObject);
                break;
            case EnemyType.HeavyShip:
                PoolManager.Instance.addInactiveHeavyShip(gameObject);
                break;
            case EnemyType.ShipCurved:
                Destroy(gameObject);
                break;


        }
    }


}

