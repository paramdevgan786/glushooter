﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "CurvedShapes", menuName = "CurvedFormationAsset")]
public class CurvedCoordinates : ScriptableObject
{

    [Header("List of simple curved waves Data")]
    [SerializeField]
    public Wave[] easyCurvedShapes;
    [Header("List of complex curved waves Data")]
    [SerializeField]
    public Wave[] complexCurvedShapes;

}

