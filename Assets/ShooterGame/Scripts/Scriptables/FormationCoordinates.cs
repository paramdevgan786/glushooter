﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "FormationCoordinates", menuName = "FormationAsset")]
public class FormationCoordinates : ScriptableObject
{
    //[Space(20)]
    // [Header("====Scriptable only handles static shapes====")]


    [Header("List of Simples shapes Data")]
    [SerializeField]
    public List<ShapeData> simpleShapes;
    [Header("List of Medium shapes Data")]
    [SerializeField]
    public List<ShapeData> mediumShapes;
    [Header("List of Complex shapes Data")]
    [SerializeField]
    public List<ShapeData> complexShapes;

}

[System.Serializable]
public class ShapeData
{
    public string shapeName;
    public ShapeType shapeType;
    public Vector3[] pathPositions;

    public ShapeData(string Name, ShapeType Type, Vector3[] Path)
    {
        shapeName = Name;
        shapeType = Type;
        pathPositions = Path;
    }

}

public enum ShapeType
{
    Simple,
    Medium,
    Complex

}