﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ShipData", menuName = "ShipGamePrefabs")]
public class Ships : ScriptableObject
{
    [SerializeField]
    public GameObject[] EnemyShipObjects;
    [SerializeField]
    public GameObject[] EnemyHeavyShip;
    [SerializeField]
    public GameObject[] EnemyBossShip;
    [SerializeField]
    public GameObject EnemyShipA;
    [SerializeField]
    public GameObject EnemyShipB;
    [SerializeField]
    public GameObject EnemyShipC;
}


