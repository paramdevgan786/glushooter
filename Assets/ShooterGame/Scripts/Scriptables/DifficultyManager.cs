﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "GameDifficultyData", menuName = "GameDifficultyAsset")]
public class DifficultyManager : ScriptableObject
{

    [Header("This value will be added to player's health in each level")]
    public int IncreaseInEnemyHealth;
    [Header("The Shoting Probablity By Enemy(0-100),following value will be added to shooting probability in each level")]
    public int IncreaseInShootingProbability;
    [Header("Increase in the speed of enemy ships")]
    public float IncreaseInShipSpeed;
    [Header("Spawn Wait time-gap between each wave")]
    public float WaveSpawnWait;

    [SerializeField]
    [Header("Add the level wise difficulty,following value will be added to player speed in each level")]
    public List<LevelDifficultyData> levelDifficulty;

    private float waveSpawnWaitTime;
    private float maxLevelCount;// this is the value which, atleast we are assuming 100 levels to buils
    public float GetWaveWaitTime()
    {
        maxLevelCount = 100;
        int levelIndex = PlayerPrefs.GetInt("SelectedLevel");
        float timeReduction = levelIndex / maxLevelCount;
        waveSpawnWaitTime = WaveSpawnWait - timeReduction;
        return waveSpawnWaitTime;
    }

}
[System.Serializable]
public class LevelDifficultyData
{
    public DifficultyData[] difficultyData;
    [Header("Other Level Related Paramters")]
    public int levelIndex;

}

[System.Serializable]
public class DifficultyData
{
    public DifficultyActionType waveActionType;
    public int occurenceOfAction;

    public DifficultyData(DifficultyActionType type, int count)
    {
        waveActionType = type;
        occurenceOfAction = count;
    }
}



