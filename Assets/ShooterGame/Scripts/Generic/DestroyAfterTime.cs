﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///Generic concept used to destroy any object after defined time
/// </summary>
public class DestroyAfterTime : MonoBehaviour
{

    [Header("Time after object will be destroyed")]
    public float destructionTime;

    private void OnEnable()
    {
        StartCoroutine(Destruction()); //launching the timer of destruction
    }

    IEnumerator Destruction() //wait for the estimated time, and destroying or deactivating the object
    {
        yield return new WaitForSeconds(destructionTime);
        Destroy(gameObject);
    }
}
