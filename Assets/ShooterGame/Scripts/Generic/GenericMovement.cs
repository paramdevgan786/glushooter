﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericMovement : MonoBehaviour
{
    public float speed;

    private Rigidbody rb;

    private void OnEnable()
    {
        rb = GetComponent<Rigidbody>();

        rb.velocity = transform.forward * speed;
    }

    public void UpdateSpeed(float newSpeed)
    {
        speed = newSpeed;
        rb.velocity = transform.forward * speed;
    }
}
