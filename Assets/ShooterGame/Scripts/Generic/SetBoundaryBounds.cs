﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetBoundaryBounds : MonoBehaviour
{

    #region PUBLIC FIELDS
    public Transform EnemyDestroyer;
    public Transform BulletDestroyer;

    #endregion


    #region private vars
    private Vector3 screenBounds;
    private float boundX;
    private float boundY;

    private float enemyDestroyerBoundaryX;
    private float enemyDestroyerBoundaryY;

    private float bulletDestroyerBoundaryY;

    #endregion


    private void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        boundX = screenBounds.x;
        boundY = screenBounds.y;

        enemyDestroyerBoundaryX = (boundX + 1) * 2;
        enemyDestroyerBoundaryY = (boundY + 1) * 2;

        bulletDestroyerBoundaryY = (boundY - 0.5f) * 2;//since we don't want bullets to damage enemies just at the border(or at creation level) so we have offser of 1 Unit 

        UpdateSize();
    }

    private void UpdateSize()
    {
        BulletDestroyer.localScale = new Vector3(enemyDestroyerBoundaryX, 5, bulletDestroyerBoundaryY);
        EnemyDestroyer.localScale = new Vector3(enemyDestroyerBoundaryX, 5, enemyDestroyerBoundaryY);
    }

}
