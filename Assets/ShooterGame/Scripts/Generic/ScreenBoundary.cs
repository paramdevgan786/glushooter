﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenBoundary : MySingleton<ScreenBoundary>
{
    #region private vars
    private Vector3 screenBounds;
    private float minX, maxX;
    private float minY, maxY;
    #endregion

    public float shipWidth { get; set; }
    public float shipHeight { get; set; }
    // Start is called before the first frame update
    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        maxX = screenBounds.x - 1f;//1 unit distance for object bounds
        maxY = screenBounds.y - 1f;//same for Y axis

        minX = maxX * (-1);
        minY = maxY * (-1);

    }

    public Vector3 GetBounds()
    {
        return screenBounds;
    }


    public float MinX
    {
        get { return minX; }
        set { minX = value; }
    }

    public float MaxX
    {
        get { return maxX; }
        set { maxX = value; }
    }

    public float MinY
    {
        get { return minY; }
        set { minY = value; }
    }

    public float MaxY
    {
        get { return maxY; }
        set { maxY = value; }
    }

}
