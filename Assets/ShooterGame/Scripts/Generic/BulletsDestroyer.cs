﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletsDestroyer : MonoBehaviour
{


    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Contains("Bullet"))
        {
            PoolManager.Instance.addInactiveMissile(other.gameObject);
        }
    }

}
