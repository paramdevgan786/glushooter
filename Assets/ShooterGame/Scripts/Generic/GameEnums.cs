﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEnums
{

}
public enum PowerUpType
{
    ShootBoost,
    Shield
}

public enum DifficultyActionType
{
    RandomWave,
    SimpleWave,
    //MediumWave,
    //ComplexWave,
    ComplexCurvedWave,
    CurvedWave,
    HeavyShipWave,
    BossWave


}

public enum EnemyType
{
    ShipA,
    ShipB,
    ShipC,
    ShipCurved,
    HeavyShip,
    BossShip

}