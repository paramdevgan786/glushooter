﻿
using UnityEngine;

public abstract class EnemyAbility : ScriptableObject
{

    [System.Serializable]
    public class EnemyShipSettings
    {
        public int Health;
        public int ShotProbability;
        public int DamageToPlayer;
        public EnemyType enemyType;
        public int scoreForPlayer;
    }


    [Header("==Enemy Ship Settings==")]
    public EnemyShipSettings shipSettings;



}
