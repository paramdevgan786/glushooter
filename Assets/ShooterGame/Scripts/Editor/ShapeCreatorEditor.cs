﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ShapeCreator))]
public class ShapeCreatorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        ShapeCreator instance = (ShapeCreator)target;

        if (GUILayout.Button("Save Coordinates"))
        {
            instance.SaveCoordinates();
        }

    }
}
