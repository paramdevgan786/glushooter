﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyDamagable
{
    void GetDamage(int damage);
}
