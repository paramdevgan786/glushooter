﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerDamagable
{
    void GetDamage(int damage);
    bool canTakeDamage { get; set; }
}
